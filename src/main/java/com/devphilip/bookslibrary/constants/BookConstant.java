package com.devphilip.bookslibrary.constants;

public class BookConstant {
    public static final Integer AVAILABLE = 1;
    public static final Integer ISSUED = 2;
}
