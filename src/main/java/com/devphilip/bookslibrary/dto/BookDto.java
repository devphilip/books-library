package com.devphilip.bookslibrary.dto;

import lombok.Data;

@Data
public class BookDto {
    private String title;
    private String authors;
    private String publisher;
    private String isbn;
    private Long categoryId;
}
