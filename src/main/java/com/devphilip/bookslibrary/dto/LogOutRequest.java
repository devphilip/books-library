package com.devphilip.bookslibrary.dto;

import lombok.Getter;

@Getter
public class LogOutRequest {
    private Long userId;
}
