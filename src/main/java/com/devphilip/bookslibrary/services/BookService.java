package com.devphilip.bookslibrary.services;

import com.devphilip.bookslibrary.dto.BookDto;
import com.devphilip.bookslibrary.entities.Book;
import com.devphilip.bookslibrary.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BookService {

	Book addBook(BookDto categoryDto);

	List<Book> findAllBooks();

	Page<Book> findAllBooksPaged(Pageable pageable);

	Optional<Book> getBook(Long id);

	Book updateBook(BookDto categoryDto, Long id);

	void deleteBook(Long id);

}
