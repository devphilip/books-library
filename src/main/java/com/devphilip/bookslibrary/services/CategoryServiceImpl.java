package com.devphilip.bookslibrary.services;

import com.devphilip.bookslibrary.dto.CategoryDto;
import com.devphilip.bookslibrary.entities.Category;
import com.devphilip.bookslibrary.exceptions.BadRequestException;
import com.devphilip.bookslibrary.exceptions.ResourceNotFoundException;
import com.devphilip.bookslibrary.repositories.CategoryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@AllArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {

	private CategoryRepository categoryRepository;

	@Override
	public Category addCategory(CategoryDto categoryDto) {
		if (StringUtils.isEmpty(categoryDto.getName())) {
			throw new BadRequestException("Category name is required");
		}
		Category categoryFromDB = categoryRepository.findByName(categoryDto.getName());
		if (categoryFromDB != null) {
			throw new BadRequestException("Category already with name " + categoryDto.getName() +" exists ");
		}

		Category category = new Category();

		category.setName(categoryDto.getName());
		category.setDescription(categoryDto.getDescription());

		return categoryRepository.save(category);
	}

	@Override
	public List<Category> findAllCategories() {
		return categoryRepository.findAll();
	}

	@Override
	public Page<Category> findAllCategoriesPaged(Pageable pageable) {
		return categoryRepository.findAll(pageable);
	}

	public Optional<Category> getCategory(Long id) {
		Optional<Category> optionalCategory = categoryRepository.findById(id);
		if (!optionalCategory.isPresent()) resourceNotFoundException(id);
		return categoryRepository.findById(id);
	}

	@Override
	public Category updateCategory(CategoryDto categoryDto, Long id) {
		Optional<Category> optionalCategory = categoryRepository.findById(id);
		if (!optionalCategory.isPresent()) resourceNotFoundException(id);
		Category category = optionalCategory.get();
		if (StringUtils.isNotEmpty(categoryDto.getName())) category.setName(categoryDto.getName());
		if (StringUtils.isNotEmpty(categoryDto.getDescription())) category.setDescription(categoryDto.getDescription());
		return categoryRepository.save(category);
	}

	@Override
	public void deleteCategory(Long id) {
		Optional<Category> optionalCategory = categoryRepository.findById(id);
		if (!optionalCategory.isPresent()) resourceNotFoundException(id);
		categoryRepository.deleteById(id);
	}

	private void resourceNotFoundException(Long id) {
		throw new ResourceNotFoundException(String.format("Could not find category with id %s.", id));
	}
	
}
