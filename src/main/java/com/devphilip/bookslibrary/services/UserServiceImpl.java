package com.devphilip.bookslibrary.services;

import com.devphilip.bookslibrary.entities.Book;
import com.devphilip.bookslibrary.entities.User;
import com.devphilip.bookslibrary.exceptions.ResourceNotFoundException;
import com.devphilip.bookslibrary.repositories.UserRepository;
import com.devphilip.bookslibrary.security.UserDetailsImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User with email: '" + email + "' Not Found"));
        return UserDetailsImpl.build(user);
    }

    @Override
    public Optional<User> getUser(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) resourceNotFoundException(id);
        return userRepository.findById(id);
    }

    @Override
    public Page<User> findAllUsersPaged(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User updateUser(User user, Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent())  resourceNotFoundException(id);
        userRepository.findById(id);
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) resourceNotFoundException(id);
        userRepository.deleteById(id);
    }

    @Override
    public Book addBookToFavorite(Long bookId, User user) {
        return null;
    }

    @Override
    public List<Book> findFavoriteBooks() {
        return null;
    }

    private void resourceNotFoundException(Long id) {
        throw new ResourceNotFoundException(String.format("Could not find user with id %s.", id));
    }

}
