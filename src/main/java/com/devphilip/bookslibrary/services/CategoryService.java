package com.devphilip.bookslibrary.services;

import com.devphilip.bookslibrary.dto.CategoryDto;
import com.devphilip.bookslibrary.entities.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

	Category addCategory(CategoryDto category);

	List<Category> findAllCategories();

	Page<Category> findAllCategoriesPaged(Pageable pageable);

	Optional<Category> getCategory(Long id);

	Category updateCategory(CategoryDto category, Long id);

	void deleteCategory(Long id);

}
