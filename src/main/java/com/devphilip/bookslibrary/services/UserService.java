package com.devphilip.bookslibrary.services;

import com.devphilip.bookslibrary.entities.Book;
import com.devphilip.bookslibrary.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {

    UserDetails loadUserByUsername(String email) throws UsernameNotFoundException;

    Optional<User> getUser(Long id);

    Page<User> findAllUsersPaged(Pageable pageable);

    List<User> findAllUsers();

    User updateUser(User user, Long id);

    void deleteUser(Long id);

    Book addBookToFavorite(Long bookId, User user);

    List<Book> findFavoriteBooks();
}
