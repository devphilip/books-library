package com.devphilip.bookslibrary.services;

import com.devphilip.bookslibrary.dto.BookDto;
import com.devphilip.bookslibrary.entities.Book;
import com.devphilip.bookslibrary.entities.Category;
import com.devphilip.bookslibrary.exceptions.BadRequestException;
import com.devphilip.bookslibrary.exceptions.ResourceNotFoundException;
import com.devphilip.bookslibrary.repositories.BookRepository;
import com.devphilip.bookslibrary.repositories.CategoryRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;
    private CategoryRepository categoryRepository;

    @Override
    public Book addBook(BookDto bookDto) {
        if (StringUtils.isEmpty(bookDto.getTitle())) throw new BadRequestException("Book Title is required");
        if (StringUtils.isEmpty(bookDto.getAuthors())) throw new BadRequestException("Book Author is required");
        if (StringUtils.isEmpty(bookDto.getIsbn())) throw new BadRequestException("Book ISBN is required");
        if (StringUtils.isEmpty(bookDto.getPublisher())) throw new BadRequestException("Book Publisher is required");
        if (bookDto.getCategoryId() <= 0) throw new BadRequestException("Book Category is required");
        Book bookFromDB = bookRepository.findByIsbn(bookDto.getIsbn());
        if (bookFromDB != null) {
            throw new BadRequestException("Book already with ISBN " + bookDto.getIsbn() + " already exists ");
        }

        Book book = new Book();

        book.setTitle(bookDto.getTitle());
        book.setAuthors(bookDto.getAuthors());
        book.setAuthors(bookDto.getAuthors());
        book.setAuthors(bookDto.getAuthors());
//		@NotNull(message = "*Please select category")
        Optional<Category> optionalCategory = categoryRepository.findById(bookDto.getCategoryId());
        if (!optionalCategory.isPresent()) resourceNotFoundException("Category", bookDto.getCategoryId());
        book.setCategory(optionalCategory.get());

        return bookRepository.save(book);
    }

    @Override
    public List<Book> findAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Page<Book> findAllBooksPaged(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    public Optional<Book> getBook(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (!optionalBook.isPresent()) resourceNotFoundException("Book", id);
        return bookRepository.findById(id);
    }

    @Override
    public Book updateBook(BookDto bookDto, Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (!optionalBook.isPresent()) resourceNotFoundException("Book", id);
        Book book = optionalBook.get();

        if (StringUtils.isEmpty(bookDto.getTitle()) || StringUtils.isEmpty(bookDto.getAuthors()) ||
                StringUtils.isEmpty(bookDto.getIsbn()) || StringUtils.isEmpty(bookDto.getPublisher()) ||
                bookDto.getCategoryId() <= 0) throw new BadRequestException("At least a field is required for update");

        if (StringUtils.isNotEmpty(bookDto.getTitle())) book.setTitle(bookDto.getTitle());
        if (StringUtils.isNotEmpty(bookDto.getAuthors())) book.setAuthors(bookDto.getAuthors());
        if (StringUtils.isNotEmpty(bookDto.getIsbn())) book.setIsbn(bookDto.getIsbn());
        if (StringUtils.isNotEmpty(bookDto.getPublisher())) book.setPublisher(bookDto.getPublisher());

        Optional<Category> optionalCategory = categoryRepository.findById(bookDto.getCategoryId());
        if (!optionalCategory.isPresent()) resourceNotFoundException("Category", bookDto.getCategoryId());
        book.setCategory(optionalCategory.get());

        return bookRepository.save(book);
    }

    @Override
    public void deleteBook(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (!optionalBook.isPresent()) resourceNotFoundException("Book", id);
        bookRepository.deleteById(id);
    }

    private void resourceNotFoundException(String propertyName, Long id) {
        throw new ResourceNotFoundException(String.format("Could not find %s with id %s.", propertyName, id));
    }

}
