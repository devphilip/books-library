package com.devphilip.bookslibrary.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Book implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String title;
	private String authors;
	private String publisher;
	private String isbn;
	private Date created;
	private Date updated;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "category_id")
	@NotNull(message = "*Please select category")
	private Category category;

	@PrePersist
	protected void onCreate() {
		created = new Date();
		updated = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}
}
