package com.devphilip.bookslibrary.entities;

public enum ERole {
    ROLE_USER,
    ROLE_LIBRARIAN,
    ROLE_ADMIN;
}
