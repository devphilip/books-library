package com.devphilip.bookslibrary.controllers;

import com.devphilip.bookslibrary.dto.BookDto;
import com.devphilip.bookslibrary.entities.Book;
import com.devphilip.bookslibrary.services.BookService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/books")
public class BookController {

	private BookService bookService;

	@PostMapping
	@Operation(summary = "Add new Book")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> updateCategory(@RequestBody BookDto bookDto) {
		Book book = bookService.addBook(bookDto);
		URI location = URI.create(String.format("/books/%s", book.getId()));
		return ResponseEntity.created(location).body(book);
	}

	@GetMapping
	@Operation(summary = "List all Books")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAllBooks(
			@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "count", defaultValue = "10", required = false) int size,
			@RequestParam(value = "order", defaultValue = "ASC", required = false) Sort.Direction direction,
			@RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortProperty) {

		try {

			if (size == 0) {
				return ResponseEntity.ok(bookService.findAllBooks());
			}
			Pageable pageable = PageRequest.of(page, size, direction, sortProperty);
			Page<Book> bookPage = bookService.findAllBooksPaged(pageable);

			Map<String, Object> response = new HashMap<>();
			response.put("books", bookPage.getContent());
			response.put("currentPage", bookPage.getNumber());
			response.put("totalItems", bookPage.getTotalElements());
			response.put("totalPages", bookPage.getTotalPages());

			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(null);
		}
	}

	@GetMapping("/{id}")
	@Operation(summary = "Get Book by id")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Optional<Book>> getBook(@PathVariable Long id) {
		return ResponseEntity.ok().body(bookService.getBook(id));
	}

	@PutMapping("/{id}")
	@Operation(summary = "Update Book's Information")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Book> updateBook(@RequestBody BookDto book, @PathVariable Long id) {
		return ResponseEntity.ok().body(bookService.updateBook(book, id));
	}

	@DeleteMapping("{/id}")
	@Operation(summary = "Delete Book by id")
//	@PreAuthorize("hasRole('ADMIN')")
	public void deleteBook(@PathVariable Long id) {
		bookService.deleteBook(id);
	}
	
}
