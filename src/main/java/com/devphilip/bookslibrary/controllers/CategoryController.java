package com.devphilip.bookslibrary.controllers;

import com.devphilip.bookslibrary.dto.CategoryDto;
import com.devphilip.bookslibrary.entities.Category;
import com.devphilip.bookslibrary.entities.Category;
import com.devphilip.bookslibrary.services.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/categories")
public class CategoryController {
	
	private CategoryService categoryService;

	@PostMapping
	@Operation(summary = "Add new Category")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> updateCategory(@RequestBody CategoryDto categoryDto) {
		Category category = categoryService.addCategory(categoryDto);
		URI location = URI.create(String.format("/categories/%s", category.getId()));
		return ResponseEntity.created(location).body(category);
	}

	@GetMapping
	@Operation(summary = "Get all Categories")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAllCategories(
			@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "count", defaultValue = "10", required = false) int size,
			@RequestParam(value = "order", defaultValue = "ASC", required = false) Sort.Direction direction,
			@RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortProperty) {

		try {

			if (size == 0) {
				return ResponseEntity.ok(categoryService.findAllCategories());
			}
			Pageable pageable = PageRequest.of(page, size, direction, sortProperty);
			Page<Category> userPage = categoryService.findAllCategoriesPaged(pageable);

			Map<String, Object> response = new HashMap<>();
			response.put("users", userPage.getContent());
			response.put("currentPage", userPage.getNumber());
			response.put("totalItems", userPage.getTotalElements());
			response.put("totalPages", userPage.getTotalPages());

			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(null);
		}
	}

	@GetMapping("/{id}")
	@Operation(summary = "Get Category by id")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Optional<Category>> getCategory(@PathVariable Long id) {
		return ResponseEntity.ok().body(categoryService.getCategory(id));
	}

	@PutMapping("/{id}")
	@Operation(summary = "Update a Category")
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Category> updateCategory(@RequestBody CategoryDto categoryDto, @PathVariable Long id) {
		return ResponseEntity.ok().body(categoryService.updateCategory(categoryDto, id));
	}

	@DeleteMapping("{/id}")
	@Operation(summary = "Delete Category by id")
//	@PreAuthorize("hasRole('ADMIN')")
	public void deleteCategory(@PathVariable Long id) {
		categoryService.deleteCategory(id);
	}

}
