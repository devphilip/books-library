package com.devphilip.bookslibrary;

import com.devphilip.bookslibrary.entities.*;
import com.devphilip.bookslibrary.repositories.BookRepository;
import com.devphilip.bookslibrary.repositories.CategoryRepository;
import com.devphilip.bookslibrary.repositories.RoleRepository;
import com.devphilip.bookslibrary.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class BooksLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(BooksLibraryApplication.class, args);
    }

    @Bean
    public CommandLineRunner runner(RoleRepository roleRepository, UserRepository userRepository,
                                    PasswordEncoder encoder, CategoryRepository categoryRepository, BookRepository bookRepository) {
        return args -> {
            roleRepository.save(new Role(ERole.ROLE_USER));
            roleRepository.save(new Role(ERole.ROLE_LIBRARIAN));
            roleRepository.save(new Role(ERole.ROLE_ADMIN));

            Set<Role> roles = new HashSet<>();
            Role userRole = roleRepository.findByName(ERole.ROLE_USER).get();
            Role libRole = roleRepository.findByName(ERole.ROLE_LIBRARIAN).get();
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN).get();
            roles.add(userRole);
            roles.add(libRole);
            roles.add(adminRole);

            User adminUser = new User("Philip", "Akpeki", "philipakpeki@gmail.com", encoder.encode("passme"));
            adminUser.setRoles(roles);
            adminUser.setActive(true);
            userRepository.save(adminUser);

            roles.remove(libRole);
            roles.remove(adminRole);
            for (int i = 0; i < 10; i++) {
                User user = new User("user_" + i + "_fn", "user_" + i + "_ln", "user_" + i + "@mail.com", encoder.encode("passme"));
                user.setRoles(roles);
                user.setActive(true);
                userRepository.save(user);
            }

            for (int i = 1; i < 4; i++) {
                Category category = new Category();
                category.setName("Category_" + i);
                category.setDescription("Category desc_" + i);
                categoryRepository.save(category);
            }

//            ((int) (Math.random()*(maximum - minimum))) + minimum;

            for (int i = 1; i <= 10; i++) {
                Book book = new Book();
                book.setTitle("Book Title_" + i);
                book.setAuthors("Book Author_" + ((int) (Math.random()*(3 - 1))) + 1);
                Category category = categoryRepository.findById(((long) (Math.random() * (3 - 1))) + 1).get();
                book.setCategory(category);
                book.setIsbn("Book ISBN_" + i);
                book.setPublisher("Book Publisher_" + ((int) (Math.random()*(3 - 1))) + 1);
                bookRepository.save(book);
            }

        };
    }

}
