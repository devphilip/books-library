package com.devphilip.bookslibrary.repositories;

import com.devphilip.bookslibrary.entities.ERole;
import com.devphilip.bookslibrary.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
