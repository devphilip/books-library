package com.devphilip.bookslibrary.repositories;

import java.util.List;

import com.devphilip.bookslibrary.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

	public List<Category> findAllByOrderByNameAsc();

    Category findByName(String name);
}
