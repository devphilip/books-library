# books-library

## This is a simple spring boot Book Library application

### Main features:
1.  User 

- login
- signup
- list all users
- find/update/delete a user by Id

2. Books

- add new book - added to category at same time
- list all books
- find/update/delete a book by Id

3. Book Categories

- add new category
- list all categories
- find/update/delete a category by Id

#### Clone the repository

git clone https://gitlab.com/devphilip/books-library.git

#### Move into the cloned directory

cd books-library

#### Run the app with maven

mvn spring-boot:run or ./mvnw spring-boot:run

### Swagger documentation

http://localhost:8080/api/book-library/swagger-ui.html

### users:
- admin user: email = philipakpeki@gmail.com password = passme
- other users: email = user_{i}@mail.com password = passme   -  i = numbers from 1 to 10
  
  e.g user_1@mail.com

###Testing from curl or postman

1. login to get access token
```
curl --request POST 'http://localhost:8080/api/book-library/auth/login'
--header 'Content-Type: application/json'
--data-raw '{
    "email":"philipakpeki@gmail.com",
    "password": "passme"
}'
```
2. pass the token in the header of any request
```
curl --request GET 'http://localhost:8080/api/book-library/books'
 --header 'Content-Type: application/json' 
 --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwaGlsaXBha3Bla2lAZ21haWwuY29tIiwiaWF0IjoxNjI5MzU1ODIwLCJleHAiOjE2MjkzNTk0MjB9.c1pFwS1nvcllGWusglP1hOPRR24tnun2dFLQU2xsZSjLJDyU_eo2DU7qyjofWGqhGZ6EmiOicnMVWtKwVHesQQ' 
 
 or 
 
 curl -X GET "http://localhost:8080/api/book-library/books?page=0&count=10&order=ASC&sortBy=id" -H  "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwaGlsaXBha3Bla2lAZ21haWwuY29tIiwiaWF0IjoxNjI5MzU1ODIwLCJleHAiOjE2MjkzNTk0MjB9.c1pFwS1nvcllGWusglP1hOPRR24tnun2dFLQU2xsZSjLJDyU_eo2DU7qyjofWGqhGZ6EmiOicnMVWtKwVHesQQ"

```
